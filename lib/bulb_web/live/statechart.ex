defmodule BulbWeb.Statechart do
  use GenServer

  def start_link(ui), do: GenServer.start_link(__MODULE__, ui)

  def init(ui) do
    {:ok, %{ui_pid: ui}}
  end
end
